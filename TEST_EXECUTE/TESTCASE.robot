*** Settings ***
Test Setup        Log To Console    <<<START>>>
Test Teardown     Log To Console    ------------------------------------------------------------------------------
Resource          ../KEYWORDS/Keywords.txt

*** Test Cases ***
WS2_ROBOT
    @{ListData}    [ER] Get Data Row For Excel    TESTDATA/DataTest.xlsx    DATA1    2
    [W] Open Browser    http://demoexex.esy.es/    chrome
    [W] Click Element    //i[@class="material-icons"]
    [W] Input Text    //input[@class="form-control"]    @{ListData}[0]
    [W] Input Text    //input[@name="password"]    @{ListData}[1]
    [W] Click Element    //button[@id="submit-login"]
    [W] Click Element    //img[@class="logo img-responsive"]
    [W] Click Element    //img[@src="http://demoexex.esy.es/img/p/2/1/21-home_default.jpg"]
    ${Namepro}=    Get Text    //h1[@class="h1"]
    FOR    ${plus}    IN RANGE    1    @{ListData}[2]
        [W] Click Element    //button[@class="btn btn-touchspin js-touchspin bootstrap-touchspin-up"]
    END
    [W] Click Element    //button[@class="btn btn-primary add-to-cart"]
    sleep    3
    [W] Click Element    //a[@class="btn btn-primary"]
    ${pricefull}=    Get Text    //*[@class="regular-price"]
    Log To Console    ราคา : ${pricefull}
    ${price}=    Get Text    //span[@class="price"]
    Log To Console    ราคา : ${price}    //span[@class="product-price"]
    ${price}=    Remove String    ${price}    $
    ${pricefull}=    Remove String    ${pricefull}    $
    ${ConvertNumber}=    Convert To Number    ${price}
    ${ConvertNumber2}=    Convert To Number    ${pricefull}
    ${Totalcal}=    Evaluate    ${ConvertNumber2} + (${ConvertNumber2} * (20)/100)
    Log To Console    ${Totalcal}
    Run Keyword If    '${Totalcal}' == '${ConvertNumber}'    Log To Console    Pass
    ...    ELSE    Log To Console    Fail
    [W] Click Element    //*[@class="btn btn-primary"]
    [W] Click Element    //*[@class="btn btn-primary continue float-xs-right"]
    [W] Click Element    //*[@name="confirmDeliveryOption"]
    [W] Click Element    //*[@class="ps-shown-by-js "]
    [W] Click Element    //*[@name="conditions_to_approve[terms-and-conditions]"]
    [W] Click Element    //*[@class="btn btn-primary center-block"]
    [W] Check Element    //*[@class="h1 card-title"]
    [EW] Write Cell Message For Excel    TESTDATA/DataTest.xlsx    DATA2    A    Name product    2    ${Namepro}
    [EW] Write Cell Message For Excel    TESTDATA/DataTest.xlsx    DATA2    B    PriceFull    2    ${pricefull}
    [EW] Write Cell Message For Excel    TESTDATA/DataTest.xlsx    DATA2    C    Price    2    ${price}
